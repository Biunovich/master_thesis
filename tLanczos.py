     1	import numpy as np
     2	import matplotlib.pyplot as plt
     3	import scipy
     4	start_vector = np.random.rand((100000))
     5	
     6	def LanczosLR(D, A, B, A_add, B_add, eps=1e-5):
     7	    n = len(A)
     8	    r = np.ones(n)/n
     9	    q0 = r.copy()
    10	    beta0 = np.linalg.norm(r)
    11	    beta = np.zeros(n)
    12	    alfa = np.zeros(n)
    13	    q = []
    14	    ev = 1
    15	    ew = 0
    16	    # tridiag = np.zeros((n,n))
    17	    pev = 0
    18	    i = 0
    19	    # while (abs(ev - pev) > eps):
    20	    while (i < n):
    21	        # q[i] = r/beta0
    22	        q.append(r/beta0)
    23	        r = D*q[i] - np.dot(A, np.dot(B, q[i])) - \
    24	            np.dot(A_add, np.dot(B_add, q[i]))
    25	        # r = np.dot(A, q[i])
    26	        r = r - q0 * beta0
    27	        alfa[i] = np.dot(q[i], r)
    28	        r = r - alfa[i]*q[i]
    29	        r = r - sum(np.dot(r, q[j])*q[j] for j in range(i))
    30	        beta[i] = np.linalg.norm(r)
    31	        # print(beta[i])
    32	        if (beta[i] < eps):
    33	            if ( i != 0):
    34	                i += 1
    35	                break
    36	        beta0 = beta[i]
    37	        q0 = q[i]
    38	        pev = ev
    39	        # ev = findMax(alfa, beta, i)
    40	        # print(ev, pev)
    41	        # ew = findEigVec(ev, alfa, beta)
    42	        i += 1
    43	    # for j in range(i):
    44	    #     tridiag[j, j]=alfa[j]
    45	    #     if (j + 1 != i):
    46	    #         tridiag[j, j + 1] = beta[j]
    47	    #         tridiag[j + 1, j] = beta[j]
    48	    # print(tridiag[:i, :i])
    49	    # ev, ew = np.linalg.eigh(tridiag[:i ,:i])
    50	    ev, ew = scipy.linalg.eigh_tridiagonal(alfa[:i], 
beta[:i-1])
    51	    # print(ev)
    52	    q = np.array(q[:i])
    53	    ew = np.dot(q[:i].T, ew)
    54	    return ev, ew
    55	
    56	
    57	def findEigVec(ev, alfa, beta):
    58	    print(beta)
    59	    ew = np.array(alfa)
    60	    ew[0] = 1
    61	    ew[1] = (ev - alfa[0])/beta[0]
    62	    for i in range(2, len(alfa)):
    63	        ew[i] = (-beta[i-2]*ew[i-2] + (ev - 
alfa[i-1])*ew[i-1])/beta[i]
    64	    return ew
    65	
    66	
    67	def findMax(alfa, beta, i):
    68	    lim = sum(alfa) + 2*sum(beta)
    69	    up = lim
    70	    low = -lim
    71	    target = 0
    72	
    73	    while(up - low > 1e-5):
    74	        mid = (up + low)/2
    75	        numBigger = numEigBigger(mid, alfa, beta, i)
    76	        if numBigger > target:
    77	            low = mid
    78	        else:
    79	            up = mid
    80	    return low
    81	
    82	
    83	def numEigBigger(mid, alfa, beta, n):
    84	    q_prev = 0
    85	    q = 1
    86	    count = 0
    87	    for i in range(n):
    88	        if i == 0:
    89	            q = mid - alfa[i]
    90	            q_prev = 1
    91	        else:
    92	            temp = q
    93	            q = (mid - alfa[i]) * q - beta[i-1]**2*q_prev
    94	            q_prev = temp
    95	        if q*q_prev < 0:
    96	            count += 1
    97	    return count
    98	
    99	
   100	if __name__ == "__main__":
   101	    # D = np.array([3,3,3,2,2])
   102	    # A = np.array([[1,0], [1,0], [1,0], [0,1], [0,1]])
   103	    # B = np.array([[1,1,1,0,0],[0,0,0,1,1]])
   104	    n = 50
   105	    m = 50
   106	    sqrtA = np.random.rand(n, n) - 0.5
   107	    A = np.dot(sqrtA, np.transpose(sqrtA))
   108	
   109	    # Lanczos(D, A, B)
   110	    e, v = LanczosLR(A, 0, 0)
   111	    ev, ew = np.linalg.eigh(A)
   112	    plt.plot(e, np.ones(m), '+')
   113	    plt.plot(ev, np.ones(n)*0.5, '+')
   114	    plt.show()
   115	    print(ev)
   116	    print(ew[4], v[4])
   117	    plt.plot(np.linspace(0, 1, num=n), ew.T[n-1], '*')
   118	    plt.plot(np.linspace(0, 1, num=n), v[m-1], '*')
   119	    plt.show()

print(__doc__)

from time import time
import Lanczos
import scipy
import numpy as np
import matplotlib.pyplot as plt
from imp import reload
from sklearn import metrics, datasets, preprocessing
from sklearn import cluster
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.preprocessing import scale
from sklearn import kernel_approximation
from sklearn import mixture
from PIL import Image
import time
import func
import imp
import dunn
imp.reload(func)
colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)
np.random.seed(42)
""" Инициализация данных одним из 5 множеств: база изображений цифр из библиотеки языка
Python, множество пикселей загружаемого изображения, множества «2 кольца», «2 полумесяца» или
выборка из смеси 3 гауссовских распределений """
#digits = load_digits()
#data = scale(digits.data)
#data=np.array(data)
#n_clusters = 10
# im_src = Image.open("bear3.jpg")
# im = np.array(list(im_src.getdata())).reshape(im_src.size[0]*im_src.size[1],3)
# data=im
# n_clusters=3
# noisy_circles = datasets.make_circles(n_samples=1000, noise=.01)
# data=noisy_circles[0]
# n_clusters = 2
noisy_moons = datasets.make_moons(n_samples=1000000, noise=.01)
data=noisy_moons[0]
n_clusters = 2
# noisy_blobs = datasets.make_blobs(n_samples=10000,cluster_std = 3)
# data=noisy_blobs[0]
# n_clusters = 3
n, n_features = data.shape
n_runs=3
classes = np.zeros((n,n_runs),dtype=int)
weights=np.zeros((n_runs))
# Построение разбиений для задачи ансамблевого кластерного анализа с помощью k-means
n_clust_for_kmeans = 10
print("creating initial partitions with k-means...")
for k in range(0,n_runs):
    clf=KMeans(init='random', n_clusters=n_clust_for_kmeans, n_init=1)
    clf.fit(data)
    pred=clf.predict(data)
    # plt.figure()
    # plt.scatter(data[:, 0], data[:, 1], color=colors[pred].tolist(), s=10)
    # plt.show()
    classes[:,k]=pred
    weights[k]=1.0/clf.inertia_
# Вычисление матриц A,B: H=AB, а также диагональной матрицы D, определенной в главе
# «Спектральный алгоритм кластерного анализа»
start=time.time()
A,B,D=func.get_coass_matr_low(classes,weights)
print("low-rank matrix computed ",time.time()-start, "seconds")
# Явное вычисление полноранговой матрицы коассоциаций и Лапласиана
start=time.time()
H=func.get_coass_matr_full(classes,weights)
L=np.diag(D)-H
print("full-rank matrix computed in ",time.time()-start, "seconds")
# Кластеризация с помощью функции библиотеки scikit-learn
# 1. Sklearn spectral clustering
print("\n--------1. Sklearn spectral clustering----------")
start=time.time()
clf = cluster.SpectralClustering(n_clusters=n_clusters, affinity='precomputed', eigen_tol=1e-10)
pred_sklearn = clf.fit_predict(H)
print("Sklearn spectral clustering with low rank: ", time.time() - start)

print("\n--------3. spectral clustering using my PI with full-rank------\n")
start=time.time()
lam, v, n_iter_full = func.PI_mult_smallest(L,n_clusters,classes)
print("PI with low rank: ", time.time() - start)

print("\n----------4. spectral clustering using my PI with low-rank--------")
start=time.time()
lam_lr,v_lr,n_iter_lr = func.PI_lapl_mult_smallest_lr(D,A,B,n_clusters,classes)
print("PI with low rank: ", time.time() - start)

print("\n----------4. spectral clustering using my Lanczos with full rank--------")
start=time.time()
lam_lr,v_lr = scipy.sparse.linalg.eigsh(L, k=n_clusters, which="SM")
print("Lanczos with full rank: ", time.time() - start)

print("\n----------4. spectral clustering using my Lanczos with low rank--------")
start=time.time()
lam_lr,v_lr = Lanczos.LanczosLR(D, A,B,0,0)
print("Lanczos with low rank: ", time.time() - start)

# print(v_lr.transpose())
v_lr = v_lr.transpose()
v_lr = v_lr[:n_clusters]
res = []
for i in  range(len(v_lr)):
    # Fit a Gaussian mixture with EM
    v = v_lr[i]
#     print(v)
    gmm = mixture.GaussianMixture(n_components=1)
    gmm2 = mixture.GaussianMixture(n_components=2)
    gmm.fit(v.reshape(-1, 1))
    # print(v.reshape(-1, 1))
    gmm2.fit(v.reshape(-1, 1))
    bic1 = gmm.bic(v.reshape(-1, 1))
    bic2 = gmm2.bic(v.reshape(-1, 1))
    print(bic2/bic1)
    if (bic2/bic1> 1):
        res.append(v)
# print(res.transpose())
res = np.array(res)
res = res.transpose()
bic = np.inf
clusters = len(res.transpose())
for i in range(1, clusters + 2):
    gmm = mixture.GaussianMixture(n_components=i)
    # print(res)
    gmm.fit(res)
    # print(gmm.bic(res))
    if (gmm.bic(res) < bic):
        bic = gmm.bic(res)
        cluster = i
        print(i)
# gmm = mixture.GaussianMixture(n_components=cluster)
# gmm.fit(res)
# pred_low = gmm.predict(res)
print(n_clusters, len(res.transpose()), cluster)
clf=KMeans(init='random', n_clusters=cluster, n_init=10)
clf.fit(res)
pred_low = clf.predict(res)
# t_low = time.time()-start
#визуализация разбиения, если данные 2-d

plt.figure()
plt.scatter(data[:, 0], data[:, 1], color=colors[pred_low].tolist(), s=10)
plt.show()

# clustered_image_sklearn=pred_low.reshape((im_src.size[1],im_src.size[0]))
# plt.figure()
# plt.imshow(clustered_image_sklearn,cmap='gray')
# plt.show()

# вывод времени работы 3 алгоритмов
# print("\nrunning time, sec: " , "%.2f" % t_sklearn, "%.2f" % t_full, "%.2f" %t_low,"\n")
# #вывод информации о времени работы, числе итераций и размерности данных в формате Таблица 2
# print("%.1e" % n, n_runs, n_clust_for_kmeans, "%.1e" % (n*n*n_runs),
# "%.1e" % (n*n_clust_for_kmeans*n_runs), n_iter_full, n_iter_lr,
# "%.2f" % t_full, "%.2f" %t_low, "%.2f" % t_sklearn,
# n/n_clust_for_kmeans/n_runs, "%.1f" % (t_full/t_low), "\n")
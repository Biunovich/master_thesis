     1	import numpy as np
     2	import matplotlib.pyplot as plt
     3	import scipy
     4	from time import time
     5	start_vector = np.random.rand((1000000))
     6	
     7	""" Функция вычисляет 
малоранговое представление матрицы 
коассоциаций.
     8	Вход:
     9	1. Classes - таблица разбиений в 
формате Таблица 1
    10	2. Weights – массив весов, отражающих 
качество разбиения
    11	Алгоритм:
    12	описан в 5.1. Алгоритм построения 
малорангового представления
    13	Выход:
    14	1. матрицы А, В
    15	2. диагональная матрица D, 
определенная в 2. Спектральный 
алгоритм кластерного анализа """
    16	
    17	def get_coass_matr_low(classes, weights):
    18	    n, n_runs = classes.shape
    19	    n_clust = np.unique(classes[:,1]).shape[0]
    20	    A_full = np.zeros((n,n_clust*n_runs))
    21	    B_full = np.zeros((n_clust*n_runs,n))
    22	    d = np.zeros(n)
    23	    for i in range(n_runs):
    24	        classes_cur = classes[:,i]
    25	        inv = (1/(classes_cur+1)).reshape(1,n)
    26	        
B=np.dot(np.arange(n_clust).reshape(n_clust,1)+1,inv)-1
    27	        B=np.logical_xor(B,np.ones((n_clust,n)))
    28	        dd = np.sum(B,axis=1)*weights[i]
    29	        A=np.zeros((n,n_clust))
    30	        A[np.arange(n),classes_cur]=weights[i]
    31	        d = d + dd[classes_cur]
    32	        #print("test of symmetry: ",np.linalg.norm(A-B.T))
    33	        A_full[:,i*n_clust:(i+1)*n_clust]=A
    34	        B_full[i*n_clust:(i+1)*n_clust,:]=B
    35	    return A_full, B_full, d
    36	
    37	""" Функция вычисляет матрицу 
коассоциаций в стандартном 
полноранговом виде.
    38	Вход:
    39	1. Classes - таблица разбиений в 
формате Таблица 1
    40	2. Weights – массив весов, отражающих 
качество разбиения
    41	Алгоритм:
    42	Прямой, по формуле ()
    43	Выход:
    44	Матрица H """
    45	
    46	def get_coass_matr_full(classes,weights):
    47	    n, n_runs = classes.shape
    48	    coass = np.zeros((n,n))
    49	    for i in range(n_runs):
    50	        classes_cur = classes[:,i]
    51	        pred1 = (1/(classes_cur+1)).reshape((n,1))
    52	        pred2=(classes_cur+1).reshape((1,n))
    53	        H=np.dot(pred1,pred2)-1
    54	        H=np.logical_xor(H,np.ones((n,n)))
    55	        coass=coass+H*weights[i]
    56	    return coass
    57	
    58	""" алгоритм степенной итерации
    59	Вход:
    60	1. A – симметричная матрица
    61	2. eps – малое число, задающее 
критерий остановки
    62	Алгоритм:
    63	Описан в 3.1. Метод степенной 
итерации
    64	Выход:
    65	1. наибольшее по модулю 
собственное число матрицы А
    66	2. соответствующий ему 
нормированный собственный вектор
    67	3. число выполненных итераций """
    68	
    69	def PI(A,eps=1e-12):
    70	    start= time()
    71	    n=A.shape[0]
    72	    #print("computing largest eigenvalue with full power 
iteration and size",n)
    73	    #x=np.ones((n))
    74	    #x=np.random.rand((n))
    75	    x=start_vector[:n]
    76	    eigval_cur = 0
    77	    eigval_new = 1
    78	    n_iter = 0
    79	    n_iter_max = 1000000
    80	    while ((abs(eigval_new-eigval_cur)>eps) & 
(n_iter<n_iter_max)):
    81	        n_iter = n_iter+1
    82	        eigval_cur = eigval_new
    83	        Ax = np.dot(A,x)
    84	        eigval_new = np.dot(x, Ax)/np.dot(x,x)
    85	        x = Ax / np.linalg.norm(Ax)
    86	        if (n_iter%10000==0):
    87	            print("PI iteration: ", 
n_iter,abs(eigval_new-eigval_cur))
    88	    print("full power iteration made ", n_iter," iterations 
in ", time()-start,"seconds")
    89	    return eigval_new, x, n_iter
    90	
    91	""" алгоритм степенной итерации 
для поиска нескольких собственных 
векторов
    92	Вход:
    93	3. A – симметричная матрица
    94	4. k – необходимое число 
собственных векторов
    95	Алгоритм:
    96	Описан в 3.2. Поиск нескольких 
минимальных собственных чисел.
    97	Выход:
    98	1. k наибольших по модулю 
собственных чисел матрицы А
    99	2. соответствующие им 
нормированные собственный векторы
   100	3. число выполненных итераций """
   101	
   102	def PI_mult(A,k):
   103	    B=A
   104	    n=A.shape[0]
   105	    values = np.zeros(k)
   106	    vectors = np.zeros((n,k))
   107	    iter_total = 0
   108	    for i in range(k):
   109	        lam, v, n_iter = PI(B)
   110	        iter_total = iter_total + n_iter
   111	        values[i]=lam
   112	        vectors[:,i]=v
   113	        B = B - lam*np.dot(v.reshape(n,1),v.reshape(1,n))
   114	    return values, vectors, iter_total
   115	
   116	""" алгоритм степенной итерации 
для поиска нескольких собственных 
векторов, соответствующих
   117	наименьшим собственным числам
   118	Вход:
   119	1. A – симметричная матрица
   120	2. k – необходимое число 
собственных векторов
   121	Алгоритм:
   122	Описан в 3.2. Поиск нескольких 
минимальных собственных чисел. 
Замечание: для поиска
   123	наименьших собственных чисел 
необходимо сначала найти наибольшее. 
В действительности,
   124	достаточно его оценки сверху. 
Поэтому функция PI(A,eps) запускается с 
достаточно слабым
   125	критерием остановки eps=1e-5.
   126	Выход:
   127	1. k наименьших по модулю 
собственных чисел матрицы А
   128	2. соответствующие им 
нормированные собственный векторы
   129	3. число выполненных итераций """
   130	
   131	def PI_mult_smallest(A, k, classes):
   132	    n=A.shape[0]
   133	    lam_largest, v_largest, n_iter_0= PI(A,eps=1e-5)
   134	    lam_largest = lam_largest+0.01
   135	    print("largest eigenvaue in full form",lam_largest)
   136	    B = A - lam_largest*np.eye(n)
   137	    values = np.zeros((k))
   138	    vectors = np.zeros((n,k))
   139	    print("searching zero eigenvalues...")
   140	    lbl = merge_all_partitions(classes)
   141	    n_comp = np.unique(lbl).shape[0]
   142	    for i in range (n_comp):
   143	        values[i]=-lam_largest
   144	        v=np.zeros((n))
   145	        v[np.where(lbl==i)[0]]=1
   146	        v = v/np.linalg.norm(v)
   147	        vectors[:,i]=v
   148	        B = B - 
values[i]*np.dot(v.reshape(n,1),v.reshape(1,n))
   149	    values[n_comp:], vectors[:,n_comp:], n_iter = 
PI_mult(B,k-n_comp)
   150	    values = values + lam_largest
   151	    return values, vectors, n_iter + n_iter_0
   152	
   153	""" алгоритм степенной итерации 
поиска наибольшего собственного 
числа матрицы вида d − AB −
   154	A_add ∙ B_add в малоранговом 
представлении
   155	Вход:
   156	1. Матрицы d, A, B, A_add, B_add из 
разложения d − AB − A_add ∙ B_add
   157	2. eps – параметр критерия 
остановки
   158	Алгоритм:
   159	Описан в 3.1. Метод степенной 
итерации.
   160	Подавая на вход матрицу D − AB = L, 
получим наибольшее собственное число 
(и вектор)
   161	Лапласиана.
   162	Выход:
   163	4. k наименьших по модулю 
собственных чисел матрицы L − T
   164	5. соответствующие им 
нормированные собственный векторы
   165	6. число выполненных итераций """
   166	
   167	def PI_lapl_lr(d,A,B,A_add,B_add,eps=1e-12):
   168	    k = A.shape[1]
   169	    n=A.shape[0]
   170	    #x=np.ones((n))
   171	    #x=np.random.rand((n))
   172	    x=start_vector[:n]
   173	    eigval_cur = 0
   174	    eigval_new = 1
   175	    n_iter = 0
   176	    n_iter_max = 1000000
   177	    start= time()
   178	    while ((abs(eigval_new-eigval_cur)>eps) & 
(n_iter<n_iter_max)):
   179	        n_iter = n_iter+1
   180	        eigval_cur = eigval_new
   181	        DABx = d*x - np.dot(A,np.dot(B,x)) - 
np.dot(A_add,np.dot(B_add,x))
   182	        #eigval_new = np.dot(x, DABx)/np.dot(x,x)
   183	        eigval_new = np.dot(x, DABx)/np.dot(x,x)
   184	        x = DABx / np.linalg.norm(DABx)
   185	        #if (n_iter%10000==0):
   186	        # print("PI iteration: ", 
n_iter,abs(eigval_new-eigval_cur))
   187	        #if (n_iter==n_iter_max):
   188	    print("low-rank power iteration made ", n_iter," 
iterations in ", "%.3f" % (time()-start),"seconds")
   189	    return eigval_new, x, n_iter
   190	
   191	""" алгоритм степенной итерации 
для вычисления нескольких наибольших 
собственных чисел
   192	матрицы d − H − T в малоранговом 
представлении
   193	Вход:
   194	3. Матрицы d, A, B, A_add, B_add из 
следующего разложения: d − H − T = d − AB 
− A_add ∙
   195	B_add. Слагаемое T = A_add ∙ B_add 
необходимо, так как в алгоритме 
поиска нескольких
   196	наименьших собственных чисел 
необходимо находить наибольшее 
собственное число
   197	матриц L − v 1 λ 1 v 1T , L − v 1 λ 1 v 1T − v 
2 λ 2 v 2T и т. д. T = v 1 λ 1 v 1T + v 2 λ 2 v 2T + ⋯
   198	4. eps – параметр критерия 
остановки
   199	Алгоритм:
   200	Описан в 3.2. Поиск нескольких 
минимальных собственных чисел.
   201	Поиск нескольких наибольших 
собственных чисел матрицы d − H − T = D 
− λ 1 I − H −
   202	v 1 λ 1 v 1T
   203	T
   204	− ⋯ − v n_comp λ n_comp v n_comp
   205	эквивалентен поиску нескольких 
наименьших собственных
   206	чисел матрицы L = D − H, начиная с 
n_comp-го наименьшего.
   207	Выход:
   208	7. k наименьших по модулю 
собственных чисел матрицы L − T
   209	8. соответствующие им 
нормированные собственный векторы
   210	9. число выполненных итераций """
   211	
   212	def PI_lapl_mult_lr(d,A,B,A_supp,B_supp,m):
   213	    k = A.shape[1]
   214	    already_computed = A_supp.shape[1]
   215	    n = A.shape[0]
   216	    A_add = np.zeros((n,m+already_computed))
   217	    B_add = np.zeros((m+already_computed,n))
   218	    A_add[:,:already_computed] = A_supp
   219	    B_add[:already_computed,:] = B_supp
   220	    values = np.zeros(m)
   221	    vectors = np.zeros((n,m))
   222	    iter_total = 0
   223	    #time_for_iter = 0
   224	    #service_time = 0
   225	    for i in range(m):
   226	        lam, v, n_iter = 
PI_lapl_lr(d,A,B,A_add[:,:already_computed+i],B_add[:already_computed+
i,:])
   227	        iter_total = iter_total + n_iter
   228	        values[i]=lam
   229	        vectors[:,i]=v
   230	        A_add[:,already_computed+i]=lam*v
   231	        B_add[i+already_computed,:]=v
   232	    return values, vectors, iter_total
   233	
   234	""" алгоритм степенной итерации 
для вычисления нескольких наименьших 
собственных чисел (и
   235	векторов) Лапласиана в 
малоранговом представлении
   236	Вход:
   237	1. Матрицы d, A, B из разложения L = D − 
H = D − AB
   238	2. k – необходимое число 
собственных векторов (и чисел)
   239	3. classes - таблица разбиений в 
задаче ансамблевого кластерного 
анализ в виде Таблица 1
   240	Алгоритм:
   241	Сначала проводится анализ 
связности графа и явно задаются 
несколько собственных чисел
   242	и векторов (параграф 3.3. 
Компоненты связности). Затем для 
модифицированной матрицы L −
   243	T
   244	v 1 λ 1 v 1T − ⋯ − v n_comp λ n_comp v n_comp
   245	вычисляются k-n_comp наибольших 
собствевнных чисел и
   246	векторов (параграф 3.2. Поиск 
нескольких минимальных собственных 
чисел).
   247	Выход:
   248	1. k наименьших по модулю 
собственных чисел матрицы L
   249	2. соответствующие им 
нормированные собственный векторы
   250	3. число выполненных итераций """
   251	
   252	def PI_lapl_mult_smallest_lr(D,A,B,k,classes):
   253	    n = A.shape[0]
   254	    print("\ncomputing largest eigenvalue by low-rank power 
iteration...")
   255	    # оценка наибольшего 
собственного числа Лапласиана
   256	    lam_largest, v_largest, n_iter0 = 
PI_lapl_lr(D,A,B,0,0,eps=1e-5)
   257	    lam_largest = lam_largest + 0.01
   258	    print("largest eigenvaue is" , lam_largest)
   259	    # переход от наибольших 
собственных чисел к наименьшим 
преобразованием L − λ 1 I = D − λ 1 I − H, 
описанным в параграфе 3.2. Поиск 
нескольких минимальных собственных 
чисел
   260	    d1 = D - lam_largest * np.ones(n)
   261	    values = np.zeros((k))
   262	    vectors = np.zeros((n,k))
   263	    print("searching zero eigenvalues...")
   264	    # Вычисление компонент 
связности графа
   265	    lbl = merge_all_partitions(classes)
   266	    n_comp = np.unique(lbl).shape[0]
   267	    A_supp = np.zeros((n,n_comp))
   268	    B_supp = np.zeros((n_comp,n))
   269	    print("Component connectivity = ", n_comp)
   270	    # Явное задание первых n_comp 
собственных чисел и векторов
   271	    for i in range (n_comp):
   272	        values[i]=-lam_largest
   273	        v=np.zeros((n))
   274	        v[np.where(lbl==i)[0]]=1
   275	        v =v/np.linalg.norm(v)
   276	        vectors[:,i]=v
   277	        A_supp[:,i] = values[i]*v
   278	        B_supp[i,:] = v
   279	    print("computing multiple smallest eigenvalues by 
low-rank power iteration...")
   280	    # Вычисление оставшихся k-n_comp 
собственных чисел и векторов
   281	    values[n_comp:], vectors[:,n_comp:], n_iter = 
PI_lapl_mult_lr(d1,A,B,A_supp,B_supp,k-n_comp)
   282	    values = values + lam_largest
   283	    return values, vectors, n_iter+n_iter0
   284	
   285	""" алгоритм слияния 2 разбиений в 
задаче поиска компонент связности 
графа
   286	Вход:
   287	1. classes1 – массив, содержащий 
номера классов в первом разбиении
   288	2. classes2 – массив, содержащий 
номера классов во втором разбиении
   289	Алгоритм:
   290	описан в 3.3. Компоненты связности.
   291	Выход:
   292	1. массив, содержащий номера 
классов в результирующем разбиении"""
   293	
   294	def merge_2_partitions(classes1, classes2):
   295	    #print(classes1)
   296	    n_clust_1 = np.unique(classes1).shape[0]
   297	    n_clust_2 = np.unique(classes2).shape[0]
   298	    #print((n_clust_1+classes2).shape)
   299	    #print(classes1.shape)
   300	    adj = np.zeros((n_clust_1+n_clust_2,n_clust_1+n_clust_2))
   301	    adj[classes1,n_clust_1+classes2]=1
   302	    n_comp, lbl = 
scipy.sparse.csgraph.connected_components(adj, directed=False)
   303	    #print("labels number ",lbl.shape)
   304	    return lbl[classes1]
   305	
   306	""" алгоритм слияния всех 
разбиений в задаче поиска компонент 
связности графа
   307	Вход:
   308	1. classes - таблица разбиений в 
задаче ансамблевого кластерного 
анализ в виде Таблица 1
   309	Алгоритм:
   310	описан в 3.3. Компоненты связности.
   311	Выход:
   312	1. массив, содержащий номера 
классов в результирующем разбиении"""
   313	
   314	def merge_all_partitions(classes):
   315	    #n=classes.shape[0]
   316	    classes_cur = classes[:,0]
   317	    for i in range(classes.shape[1]-1):
   318	        classes_cur = 
merge_2_partitions(classes_cur,classes[:,i+1])
   319	    return classes_cur
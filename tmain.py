     1	print(__doc__)
     2	
     3	from time import time
     4	import Lanczos
     5	import scipy
     6	import numpy as np
     7	import matplotlib.pyplot as plt
     8	from imp import reload
     9	from sklearn import metrics, datasets, preprocessing
    10	from sklearn import cluster
    11	from sklearn.cluster import KMeans
    12	from sklearn.datasets import load_digits
    13	from sklearn.preprocessing import scale
    14	from sklearn import kernel_approximation
    15	from sklearn import mixture
    16	from PIL import Image
    17	import time
    18	import func
    19	import imp
    20	import dunn
    21	imp.reload(func)
    22	colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
    23	colors = np.hstack([colors] * 20)
    24	np.random.seed(42)
    25	""" Инициализация данных одним из 5 
множеств: база изображений цифр из 
библиотеки языка
    26	Python, множество пикселей 
загружаемого изображения, множества 
«2 кольца», «2 полумесяца» или
    27	выборка из смеси 3 гауссовских 
распределений """
    28	#digits = load_digits()
    29	#data = scale(digits.data)
    30	#data=np.array(data)
    31	#n_clusters = 10
    32	# im_src = Image.open("bear3.jpg")
    33	# im = 
np.array(list(im_src.getdata())).reshape(im_src.size[0]*im_src.size[1]
,3)
    34	# data=im
    35	# n_clusters=3
    36	# noisy_circles = datasets.make_circles(n_samples=1000, 
noise=.01)
    37	# data=noisy_circles[0]
    38	# n_clusters = 2
    39	noisy_moons = datasets.make_moons(n_samples=1000000, 
noise=.01)
    40	data=noisy_moons[0]
    41	n_clusters = 2
    42	# noisy_blobs = 
datasets.make_blobs(n_samples=10000,cluster_std = 3)
    43	# data=noisy_blobs[0]
    44	# n_clusters = 3
    45	n, n_features = data.shape
    46	n_runs=3
    47	classes = np.zeros((n,n_runs),dtype=int)
    48	weights=np.zeros((n_runs))
    49	# Построение разбиений для задачи 
ансамблевого кластерного анализа с 
помощью k-means
    50	n_clust_for_kmeans = 10
    51	print("creating initial partitions with k-means...")
    52	for k in range(0,n_runs):
    53	    clf=KMeans(init='random', n_clusters=n_clust_for_kmeans, 
n_init=1)
    54	    clf.fit(data)
    55	    pred=clf.predict(data)
    56	    # plt.figure()
    57	    # plt.scatter(data[:, 0], data[:, 1], 
color=colors[pred].tolist(), s=10)
    58	    # plt.show()
    59	    classes[:,k]=pred
    60	    weights[k]=1.0/clf.inertia_
    61	# Вычисление матриц A,B: H=AB, а также 
диагональной матрицы D, определенной 
в главе
    62	# «Спектральный алгоритм 
кластерного анализа»
    63	start=time.time()
    64	A,B,D=func.get_coass_matr_low(classes,weights)
    65	print("low-rank matrix computed ",time.time()-start, 
"seconds")
    66	# Явное вычисление полноранговой 
матрицы коассоциаций и Лапласиана
    67	start=time.time()
    68	H=func.get_coass_matr_full(classes,weights)
    69	L=np.diag(D)-H
    70	print("full-rank matrix computed in ",time.time()-start, 
"seconds")
    71	# Кластеризация с помощью функции 
библиотеки scikit-learn
    72	# 1. Sklearn spectral clustering
    73	print("\n--------1. Sklearn spectral clustering----------")
    74	start=time.time()
    75	clf = cluster.SpectralClustering(n_clusters=n_clusters, 
affinity='precomputed', eigen_tol=1e-10)
    76	pred_sklearn = clf.fit_predict(H)
    77	print("Sklearn spectral clustering with low rank: ", 
time.time() - start)
    78	
    79	print("\n--------3. spectral clustering using my PI with 
full-rank------\n")
    80	start=time.time()
    81	lam, v, n_iter_full = 
func.PI_mult_smallest(L,n_clusters,classes)
    82	print("PI with low rank: ", time.time() - start)
    83	
    84	print("\n----------4. spectral clustering using my PI with 
low-rank--------")
    85	start=time.time()
    86	lam_lr,v_lr,n_iter_lr = 
func.PI_lapl_mult_smallest_lr(D,A,B,n_clusters,classes)
    87	print("PI with low rank: ", time.time() - start)
    88	
    89	print("\n----------4. spectral clustering using my Lanczos 
with full rank--------")
    90	start=time.time()
    91	lam_lr,v_lr = scipy.sparse.linalg.eigsh(L, k=n_clusters, 
which="SM")
    92	print("Lanczos with full rank: ", time.time() - start)
    93	
    94	print("\n----------4. spectral clustering using my Lanczos 
with low rank--------")
    95	start=time.time()
    96	lam_lr,v_lr = Lanczos.LanczosLR(D, A,B,0,0)
    97	print("Lanczos with low rank: ", time.time() - start)
    98	
    99	# print(v_lr.transpose())
   100	v_lr = v_lr.transpose()
   101	v_lr = v_lr[:n_clusters]
   102	res = []
   103	for i in  range(len(v_lr)):
   104	    # Fit a Gaussian mixture with EM
   105	    v = v_lr[i]
   106	#     print(v)
   107	    gmm = mixture.GaussianMixture(n_components=1)
   108	    gmm2 = mixture.GaussianMixture(n_components=2)
   109	    gmm.fit(v.reshape(-1, 1))
   110	    # print(v.reshape(-1, 1))
   111	    gmm2.fit(v.reshape(-1, 1))
   112	    bic1 = gmm.bic(v.reshape(-1, 1))
   113	    bic2 = gmm2.bic(v.reshape(-1, 1))
   114	    print(bic2/bic1)
   115	    if (bic2/bic1> 1):
   116	        res.append(v)
   117	# print(res.transpose())
   118	res = np.array(res)
   119	res = res.transpose()
   120	bic = np.inf
   121	clusters = len(res.transpose())
   122	for i in range(1, clusters + 2):
   123	    gmm = mixture.GaussianMixture(n_components=i)
   124	    # print(res)
   125	    gmm.fit(res)
   126	    # print(gmm.bic(res))
   127	    if (gmm.bic(res) < bic):
   128	        bic = gmm.bic(res)
   129	        cluster = i
   130	        print(i)
   131	# gmm = mixture.GaussianMixture(n_components=cluster)
   132	# gmm.fit(res)
   133	# pred_low = gmm.predict(res)
   134	print(n_clusters, len(res.transpose()), cluster)
   135	clf=KMeans(init='random', n_clusters=cluster, n_init=10)
   136	clf.fit(res)
   137	pred_low = clf.predict(res)
   138	# t_low = time.time()-start
   139	#визуализация разбиения, если 
данные 2-d
   140	
   141	plt.figure()
   142	plt.scatter(data[:, 0], data[:, 1], 
color=colors[pred_low].tolist(), s=10)
   143	plt.show()
   144	
   145	# 
clustered_image_sklearn=pred_low.reshape((im_src.size[1],im_src.size[0
]))
   146	# plt.figure()
   147	# plt.imshow(clustered_image_sklearn,cmap='gray')
   148	# plt.show()
   149	
   150	# вывод времени работы 3 
алгоритмов
   151	# print("\nrunning time, sec: " , "%.2f" % t_sklearn, "%.2f" 
% t_full, "%.2f" %t_low,"\n")
   152	# #вывод информации о времени 
работы, числе итераций и размерности 
данных в формате Таблица 2
   153	# print("%.1e" % n, n_runs, n_clust_for_kmeans, "%.1e" % 
(n*n*n_runs),
   154	# "%.1e" % (n*n_clust_for_kmeans*n_runs), n_iter_full, 
n_iter_lr,
   155	# "%.2f" % t_full, "%.2f" %t_low, "%.2f" % t_sklearn,
   156	# n/n_clust_for_kmeans/n_runs, "%.1f" % (t_full/t_low), "\n")
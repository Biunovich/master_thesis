import numpy as np
import matplotlib.pyplot as plt
import scipy
from time import time
start_vector = np.random.rand((1000000))

""" Функция вычисляет малоранговое представление матрицы коассоциаций.
Вход:
1. Classes - таблица разбиений в формате Таблица 1
2. Weights – массив весов, отражающих качество разбиения
Алгоритм:
описан в 5.1. Алгоритм построения малорангового представления
Выход:
1. матрицы А, В
2. диагональная матрица D, определенная в 2. Спектральный алгоритм кластерного анализа """

def get_coass_matr_low(classes, weights):
    n, n_runs = classes.shape
    n_clust = np.unique(classes[:,1]).shape[0]
    A_full = np.zeros((n,n_clust*n_runs))
    B_full = np.zeros((n_clust*n_runs,n))
    d = np.zeros(n)
    for i in range(n_runs):
        classes_cur = classes[:,i]
        inv = (1/(classes_cur+1)).reshape(1,n)
        B=np.dot(np.arange(n_clust).reshape(n_clust,1)+1,inv)-1
        B=np.logical_xor(B,np.ones((n_clust,n)))
        dd = np.sum(B,axis=1)*weights[i]
        A=np.zeros((n,n_clust))
        A[np.arange(n),classes_cur]=weights[i]
        d = d + dd[classes_cur]
        #print("test of symmetry: ",np.linalg.norm(A-B.T))
        A_full[:,i*n_clust:(i+1)*n_clust]=A
        B_full[i*n_clust:(i+1)*n_clust,:]=B
    return A_full, B_full, d

""" Функция вычисляет матрицу коассоциаций в стандартном полноранговом виде.
Вход:
1. Classes - таблица разбиений в формате Таблица 1
2. Weights – массив весов, отражающих качество разбиения
Алгоритм:
Прямой, по формуле ()
Выход:
Матрица H """

def get_coass_matr_full(classes,weights):
    n, n_runs = classes.shape
    coass = np.zeros((n,n))
    for i in range(n_runs):
        classes_cur = classes[:,i]
        pred1 = (1/(classes_cur+1)).reshape((n,1))
        pred2=(classes_cur+1).reshape((1,n))
        H=np.dot(pred1,pred2)-1
        H=np.logical_xor(H,np.ones((n,n)))
        coass=coass+H*weights[i]
    return coass

""" алгоритм степенной итерации
Вход:
1. A – симметричная матрица
2. eps – малое число, задающее критерий остановки
Алгоритм:
Описан в 3.1. Метод степенной итерации
Выход:
1. наибольшее по модулю собственное число матрицы А
2. соответствующий ему нормированный собственный вектор
3. число выполненных итераций """

def PI(A,eps=1e-12):
    start= time()
    n=A.shape[0]
    #print("computing largest eigenvalue with full power iteration and size",n)
    #x=np.ones((n))
    #x=np.random.rand((n))
    x=start_vector[:n]
    eigval_cur = 0
    eigval_new = 1
    n_iter = 0
    n_iter_max = 1000000
    while ((abs(eigval_new-eigval_cur)>eps) & (n_iter<n_iter_max)):
        n_iter = n_iter+1
        eigval_cur = eigval_new
        Ax = np.dot(A,x)
        eigval_new = np.dot(x, Ax)/np.dot(x,x)
        x = Ax / np.linalg.norm(Ax)
        if (n_iter%10000==0):
            print("PI iteration: ", n_iter,abs(eigval_new-eigval_cur))
    print("full power iteration made ", n_iter," iterations in ", time()-start,"seconds")
    return eigval_new, x, n_iter

""" алгоритм степенной итерации для поиска нескольких собственных векторов
Вход:
3. A – симметричная матрица
4. k – необходимое число собственных векторов
Алгоритм:
Описан в 3.2. Поиск нескольких минимальных собственных чисел.
Выход:
1. k наибольших по модулю собственных чисел матрицы А
2. соответствующие им нормированные собственный векторы
3. число выполненных итераций """

def PI_mult(A,k):
    B=A
    n=A.shape[0]
    values = np.zeros(k)
    vectors = np.zeros((n,k))
    iter_total = 0
    for i in range(k):
        lam, v, n_iter = PI(B)
        iter_total = iter_total + n_iter
        values[i]=lam
        vectors[:,i]=v
        B = B - lam*np.dot(v.reshape(n,1),v.reshape(1,n))
    return values, vectors, iter_total

""" алгоритм степенной итерации для поиска нескольких собственных векторов, соответствующих
наименьшим собственным числам
Вход:
1. A – симметричная матрица
2. k – необходимое число собственных векторов
Алгоритм:
Описан в 3.2. Поиск нескольких минимальных собственных чисел. Замечание: для поиска
наименьших собственных чисел необходимо сначала найти наибольшее. В действительности,
достаточно его оценки сверху. Поэтому функция PI(A,eps) запускается с достаточно слабым
критерием остановки eps=1e-5.
Выход:
1. k наименьших по модулю собственных чисел матрицы А
2. соответствующие им нормированные собственный векторы
3. число выполненных итераций """

def PI_mult_smallest(A, k, classes):
    n=A.shape[0]
    lam_largest, v_largest, n_iter_0= PI(A,eps=1e-5)
    lam_largest = lam_largest+0.01
    print("largest eigenvaue in full form",lam_largest)
    B = A - lam_largest*np.eye(n)
    values = np.zeros((k))
    vectors = np.zeros((n,k))
    print("searching zero eigenvalues...")
    lbl = merge_all_partitions(classes)
    n_comp = np.unique(lbl).shape[0]
    for i in range (n_comp):
        values[i]=-lam_largest
        v=np.zeros((n))
        v[np.where(lbl==i)[0]]=1
        v = v/np.linalg.norm(v)
        vectors[:,i]=v
        B = B - values[i]*np.dot(v.reshape(n,1),v.reshape(1,n))
    values[n_comp:], vectors[:,n_comp:], n_iter = PI_mult(B,k-n_comp)
    values = values + lam_largest
    return values, vectors, n_iter + n_iter_0

""" алгоритм степенной итерации поиска наибольшего собственного числа матрицы вида d − AB −
A_add ∙ B_add в малоранговом представлении
Вход:
1. Матрицы d, A, B, A_add, B_add из разложения d − AB − A_add ∙ B_add
2. eps – параметр критерия остановки
Алгоритм:
Описан в 3.1. Метод степенной итерации.
Подавая на вход матрицу D − AB = L, получим наибольшее собственное число (и вектор)
Лапласиана.
Выход:
4. k наименьших по модулю собственных чисел матрицы L − T
5. соответствующие им нормированные собственный векторы
6. число выполненных итераций """

def PI_lapl_lr(d,A,B,A_add,B_add,eps=1e-12):
    k = A.shape[1]
    n=A.shape[0]
    #x=np.ones((n))
    #x=np.random.rand((n))
    x=start_vector[:n]
    eigval_cur = 0
    eigval_new = 1
    n_iter = 0
    n_iter_max = 1000000
    start= time()
    while ((abs(eigval_new-eigval_cur)>eps) & (n_iter<n_iter_max)):
        n_iter = n_iter+1
        eigval_cur = eigval_new
        DABx = d*x - np.dot(A,np.dot(B,x)) - np.dot(A_add,np.dot(B_add,x))
        #eigval_new = np.dot(x, DABx)/np.dot(x,x)
        eigval_new = np.dot(x, DABx)/np.dot(x,x)
        x = DABx / np.linalg.norm(DABx)
        #if (n_iter%10000==0):
        # print("PI iteration: ", n_iter,abs(eigval_new-eigval_cur))
        #if (n_iter==n_iter_max):
    print("low-rank power iteration made ", n_iter," iterations in ", "%.3f" % (time()-start),"seconds")
    return eigval_new, x, n_iter

""" алгоритм степенной итерации для вычисления нескольких наибольших собственных чисел
матрицы d − H − T в малоранговом представлении
Вход:
3. Матрицы d, A, B, A_add, B_add из следующего разложения: d − H − T = d − AB − A_add ∙
B_add. Слагаемое T = A_add ∙ B_add необходимо, так как в алгоритме поиска нескольких
наименьших собственных чисел необходимо находить наибольшее собственное число
матриц L − v 1 λ 1 v 1T , L − v 1 λ 1 v 1T − v 2 λ 2 v 2T и т. д. T = v 1 λ 1 v 1T + v 2 λ 2 v 2T + ⋯
4. eps – параметр критерия остановки
Алгоритм:
Описан в 3.2. Поиск нескольких минимальных собственных чисел.
Поиск нескольких наибольших собственных чисел матрицы d − H − T = D − λ 1 I − H −
v 1 λ 1 v 1T
T
− ⋯ − v n_comp λ n_comp v n_comp
эквивалентен поиску нескольких наименьших собственных
чисел матрицы L = D − H, начиная с n_comp-го наименьшего.
Выход:
7. k наименьших по модулю собственных чисел матрицы L − T
8. соответствующие им нормированные собственный векторы
9. число выполненных итераций """

def PI_lapl_mult_lr(d,A,B,A_supp,B_supp,m):
    k = A.shape[1]
    already_computed = A_supp.shape[1]
    n = A.shape[0]
    A_add = np.zeros((n,m+already_computed))
    B_add = np.zeros((m+already_computed,n))
    A_add[:,:already_computed] = A_supp
    B_add[:already_computed,:] = B_supp
    values = np.zeros(m)
    vectors = np.zeros((n,m))
    iter_total = 0
    #time_for_iter = 0
    #service_time = 0
    for i in range(m):
        lam, v, n_iter = PI_lapl_lr(d,A,B,A_add[:,:already_computed+i],B_add[:already_computed+i,:])
        iter_total = iter_total + n_iter
        values[i]=lam
        vectors[:,i]=v
        A_add[:,already_computed+i]=lam*v
        B_add[i+already_computed,:]=v
    return values, vectors, iter_total

""" алгоритм степенной итерации для вычисления нескольких наименьших собственных чисел (и
векторов) Лапласиана в малоранговом представлении
Вход:
1. Матрицы d, A, B из разложения L = D − H = D − AB
2. k – необходимое число собственных векторов (и чисел)
3. classes - таблица разбиений в задаче ансамблевого кластерного анализ в виде Таблица 1
Алгоритм:
Сначала проводится анализ связности графа и явно задаются несколько собственных чисел
и векторов (параграф 3.3. Компоненты связности). Затем для модифицированной матрицы L −
T
v 1 λ 1 v 1T − ⋯ − v n_comp λ n_comp v n_comp
вычисляются k-n_comp наибольших собствевнных чисел и
векторов (параграф 3.2. Поиск нескольких минимальных собственных чисел).
Выход:
1. k наименьших по модулю собственных чисел матрицы L
2. соответствующие им нормированные собственный векторы
3. число выполненных итераций """

def PI_lapl_mult_smallest_lr(D,A,B,k,classes):
    n = A.shape[0]
    print("\ncomputing largest eigenvalue by low-rank power iteration...")
    # оценка наибольшего собственного числа Лапласиана
    lam_largest, v_largest, n_iter0 = PI_lapl_lr(D,A,B,0,0,eps=1e-5)
    lam_largest = lam_largest + 0.01
    print("largest eigenvaue is" , lam_largest)
    # переход от наибольших собственных чисел к наименьшим преобразованием L − λ 1 I = D − λ 1 I − H, описанным в параграфе 3.2. Поиск нескольких минимальных собственных чисел
    d1 = D - lam_largest * np.ones(n)
    values = np.zeros((k))
    vectors = np.zeros((n,k))
    print("searching zero eigenvalues...")
    # Вычисление компонент связности графа
    lbl = merge_all_partitions(classes)
    n_comp = np.unique(lbl).shape[0]
    A_supp = np.zeros((n,n_comp))
    B_supp = np.zeros((n_comp,n))
    print("Component connectivity = ", n_comp)
    # Явное задание первых n_comp собственных чисел и векторов
    for i in range (n_comp):
        values[i]=-lam_largest
        v=np.zeros((n))
        v[np.where(lbl==i)[0]]=1
        v =v/np.linalg.norm(v)
        vectors[:,i]=v
        A_supp[:,i] = values[i]*v
        B_supp[i,:] = v
    print("computing multiple smallest eigenvalues by low-rank power iteration...")
    # Вычисление оставшихся k-n_comp собственных чисел и векторов
    values[n_comp:], vectors[:,n_comp:], n_iter = PI_lapl_mult_lr(d1,A,B,A_supp,B_supp,k-n_comp)
    values = values + lam_largest
    return values, vectors, n_iter+n_iter0

""" алгоритм слияния 2 разбиений в задаче поиска компонент связности графа
Вход:
1. classes1 – массив, содержащий номера классов в первом разбиении
2. classes2 – массив, содержащий номера классов во втором разбиении
Алгоритм:
описан в 3.3. Компоненты связности.
Выход:
1. массив, содержащий номера классов в результирующем разбиении"""

def merge_2_partitions(classes1, classes2):
    #print(classes1)
    n_clust_1 = np.unique(classes1).shape[0]
    n_clust_2 = np.unique(classes2).shape[0]
    #print((n_clust_1+classes2).shape)
    #print(classes1.shape)
    adj = np.zeros((n_clust_1+n_clust_2,n_clust_1+n_clust_2))
    adj[classes1,n_clust_1+classes2]=1
    n_comp, lbl = scipy.sparse.csgraph.connected_components(adj, directed=False)
    #print("labels number ",lbl.shape)
    return lbl[classes1]

""" алгоритм слияния всех разбиений в задаче поиска компонент связности графа
Вход:
1. classes - таблица разбиений в задаче ансамблевого кластерного анализ в виде Таблица 1
Алгоритм:
описан в 3.3. Компоненты связности.
Выход:
1. массив, содержащий номера классов в результирующем разбиении"""

def merge_all_partitions(classes):
    #n=classes.shape[0]
    classes_cur = classes[:,0]
    for i in range(classes.shape[1]-1):
        classes_cur = merge_2_partitions(classes_cur,classes[:,i+1])
    return classes_cur
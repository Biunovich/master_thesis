import numpy as np
import matplotlib.pyplot as plt
import scipy
start_vector = np.random.rand((100000))

def LanczosLR(D, A, B, A_add, B_add, eps=1e-5):
    n = len(A)
    r = np.ones(n)/n
    q0 = r.copy()
    beta0 = np.linalg.norm(r)
    beta = np.zeros(n)
    alfa = np.zeros(n)
    q = []
    ev = 1
    ew = 0
    # tridiag = np.zeros((n,n))
    pev = 0
    i = 0
    # while (abs(ev - pev) > eps):
    while (i < n):
        # q[i] = r/beta0
        q.append(r/beta0)
        r = D*q[i] - np.dot(A, np.dot(B, q[i])) - \
            np.dot(A_add, np.dot(B_add, q[i]))
        # r = np.dot(A, q[i])
        r = r - q0 * beta0
        alfa[i] = np.dot(q[i], r)
        r = r - alfa[i]*q[i]
        r = r - sum(np.dot(r, q[j])*q[j] for j in range(i))
        beta[i] = np.linalg.norm(r)
        # print(beta[i])
        if (beta[i] < eps):
            if ( i != 0):
                i += 1
                break
        beta0 = beta[i]
        q0 = q[i]
        pev = ev
        # ev = findMax(alfa, beta, i)
        # print(ev, pev)
        # ew = findEigVec(ev, alfa, beta)
        i += 1
    # for j in range(i):
    #     tridiag[j, j]=alfa[j]
    #     if (j + 1 != i):
    #         tridiag[j, j + 1] = beta[j]
    #         tridiag[j + 1, j] = beta[j]
    # print(tridiag[:i, :i])
    # ev, ew = np.linalg.eigh(tridiag[:i ,:i])
    ev, ew = scipy.linalg.eigh_tridiagonal(alfa[:i], beta[:i-1])
    # print(ev)
    q = np.array(q[:i])
    ew = np.dot(q[:i].T, ew)
    return ev, ew


def findEigVec(ev, alfa, beta):
    print(beta)
    ew = np.array(alfa)
    ew[0] = 1
    ew[1] = (ev - alfa[0])/beta[0]
    for i in range(2, len(alfa)):
        ew[i] = (-beta[i-2]*ew[i-2] + (ev - alfa[i-1])*ew[i-1])/beta[i]
    return ew


def findMax(alfa, beta, i):
    lim = sum(alfa) + 2*sum(beta)
    up = lim
    low = -lim
    target = 0

    while(up - low > 1e-5):
        mid = (up + low)/2
        numBigger = numEigBigger(mid, alfa, beta, i)
        if numBigger > target:
            low = mid
        else:
            up = mid
    return low


def numEigBigger(mid, alfa, beta, n):
    q_prev = 0
    q = 1
    count = 0
    for i in range(n):
        if i == 0:
            q = mid - alfa[i]
            q_prev = 1
        else:
            temp = q
            q = (mid - alfa[i]) * q - beta[i-1]**2*q_prev
            q_prev = temp
        if q*q_prev < 0:
            count += 1
    return count


if __name__ == "__main__":
    # D = np.array([3,3,3,2,2])
    # A = np.array([[1,0], [1,0], [1,0], [0,1], [0,1]])
    # B = np.array([[1,1,1,0,0],[0,0,0,1,1]])
    n = 50
    m = 50
    sqrtA = np.random.rand(n, n) - 0.5
    A = np.dot(sqrtA, np.transpose(sqrtA))

    # Lanczos(D, A, B)
    e, v = LanczosLR(A, 0, 0)
    ev, ew = np.linalg.eigh(A)
    plt.plot(e, np.ones(m), '+')
    plt.plot(ev, np.ones(n)*0.5, '+')
    plt.show()
    print(ev)
    print(ew[4], v[4])
    plt.plot(np.linspace(0, 1, num=n), ew.T[n-1], '*')
    plt.plot(np.linspace(0, 1, num=n), v[m-1], '*')
    plt.show()
